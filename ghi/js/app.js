function createCard(name, description, pictureUrl, date_start, date_end, location){
    return`
        <div class="card shadow-sm p-3 mb-5 bg-body-tertiary rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
            ${date_start}-${date_end}
            </div>
        </div>
    `
}



window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try{
        const response = await fetch(url);
        if (!response.ok){

        } else {
            const data = await response.json();
            console.log(data);
            for (let conference of data.conferences){
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok){
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const options = {year: 'numeric', month: 'numeric', day: 'numeric' };
                    const date_start = new Date(details.conference.starts).toLocaleDateString('en-US',options);
                    const date_end = new Date(details.conference.ends).toLocaleDateString('en-US',options);
                    const location = details.conference.location.name;
                    const html = createCard(name, description, pictureUrl,date_start, date_end, location);
                    console.log(name);
                    console.log(date_start);
                    const column = document.querySelector('.row');
                    column.innerHTML += html;
                }
            }

        }
    } catch (e) {

    }

});
