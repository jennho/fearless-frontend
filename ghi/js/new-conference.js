document.addEventListener("DOMContentLoaded", async ()=>{
    const url = "http://localhost:8000/api/locations/";
    const response = await fetch(url);

    if (response.ok){
        const data = await response.json();
        console.log(data)

        const selectTag = document.getElementById("location");

        for (let location of data.locations){

            const optionElement = document.createElement("option");
            optionElement.value = location.id;
            optionElement.innerHTML = location.name;
            selectTag.appendChild(optionElement);
        }

        //identify the form tag
        const form = document.getElementById("create-conference-form");

        // listen for a submission
        form.addEventListener('submit', async (e) => {
            //prevent the form from returning an HTTP response
            e.preventDefault();

            //create FormData object, which creates key value pairs from the form
            const formData = new FormData(form);

            //create JS Object with Object.fromEntries and then make that JSON
            const json = JSON.stringify(Object.fromEntries(formData));

            const conferenceUrl = 'http://localhost:8000/api/conferences/';
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(conferenceUrl, fetchConfig);
            console.log(response);

            if (response.ok) {
                form.reset();
                const newConference = await response.json();
            }

        })

    } else {
        throw new Error("Response not ok")
    }
})
