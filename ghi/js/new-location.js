document.addEventListener("DOMContentLoaded", async ()=>{
    const url = "http://localhost:8000/api/states/";
    const response = await fetch(url);

    if (response.ok){
        const data = await response.json();
        console.log(data)

        // Once you have the data, get teh select element with its id "state"
        const selectTag = document.getElementById("state");

        // for each state in the states property
        for (let state of data.states){
            //create an option item to hold each item
            const optionElement = document.createElement("option");

            optionElement.value = state.abbreviation;
            optionElement.innerHTML = state.name;
            selectTag.appendChild(optionElement);
        }

        //identify the form tag
        const form = document.getElementById("create-location-form");

        // listen for a submission
        form.addEventListener('submit', async (e) => {
            //prevent the form from returning an HTTP response
            e.preventDefault;

            //create FormData object, which creates key value pairs from the form
            const formData = new FormData(form);

            //create JS Object with Object.fromEntries and then make that JSON
            const json = JSON.stringify(Object.fromEntries(formData));

            const locationUrl = 'http://localhost:8000/api/locations/';
            const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
            };
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
            console.log(newLocation);
            }

        })

    } else {
        throw new Error("Response not ok")
    }
})
